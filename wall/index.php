<?php

require_once $_SERVER['DOCUMENT_ROOT'] . "/errors.php";

global $_KEY;

if (isset($_GET['v'])) {
    if (strpos($_GET['v'], "/") !== false || strpos($_GET['v'], "?") !== false || strpos($_GET['v'], "%") !== false || strpos($_GET['v'], "&") !== false) {
        header("Location: /");
        die();
    }
    if ($_GET['v'] == "last") {
        $branch = "master";
    } else {
        $branch = "Plasma/" . $_GET['v'];
    }
} else {
    header("Location: /");
    die();
}

$branches_raw = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/cache/branches.json"));

$branches_names = [];
foreach ($branches_raw as $sbranch) {
    if (substr($sbranch->name, 0, 7) == "Plasma/") {
        array_push($branches_names, $sbranch->name);
    }
}

sort($branches_names, SORT_NATURAL);
$branches_names = array_reverse($branches_names);

if ($branch != "master" && !in_array($branch, $branches_names)) {
    header("Location: /");
    die();
}

if ($_GET['v'] == "last") {
    $_TITLE = "Latest Development Branch";
} else {
    $_TITLE = "Plasma " . $_GET['v'];
}

$inf = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/cache/branch-" . str_replace("/", "_-_", $branch) . ".json"));

$names = [];

foreach ($inf as $file) {
    if (substr($file->name, 0, 10) != "base_size." && substr($file->name, 0, 19) != "vertical_base_size.") {
        array_push($names, $file->name);
    }
}

sort($names, SORT_NATURAL);
$best = explode(".", $names[count($names)-1])[0];

require_once $_SERVER['DOCUMENT_ROOT'] . "/private/header.php"; ?>

<div id="display-outer">
    <div id="display" style="background-image:url('/preview.php?v=<?= $_GET['v'] ?>')">
    </div>
</div>

<div id="box">
    <div>
        <img src="/preview.php?v=<?= $_GET['v'] ?>" height="200px">
        <h1><?php

            if ($_GET['v'] == "last") {
                echo("Latest Development Branch");
            } else {
                echo("Plasma " . $_GET['v']);
            }

            ?></h1>
        <p><a type="button" href="/download.php?v=<?= $_GET['v']?>&res=<?= $best ?>" class="btn btn-primary">
            Download highest resolution (<?= str_replace("x", "×", $best) ?>)
        </a></p>
        <span class="dropdown">
            <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown">
                Portrait Downloads
            </button>
            <div class="dropdown-menu">
                <div style="display:grid;grid-template-columns:1fr 1fr;"><?php

                    foreach ($names as $file) {
                        $p = explode(".", $file)[0];
                        $els = explode("x", $p);
                        if ($els[1] > $els[0]) { // It's portrait
                            echo("<a class='dropdown-item' href='/download.php?v=" . $_GET['v'] . "&res=" . $p . "'>" . str_replace("x", "×", $p) . "</a>");
                        } else { // It's landscape

                        }
                    }

                    ?></div>
            </div>
        </span>
        <span class="dropdown">
            <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown">
                Landscape Downloads
            </button>
            <div class="dropdown-menu">
                <div style="display:grid;grid-template-columns:1fr 1fr;"><?php

                foreach ($names as $file) {
                    $p = explode(".", $file)[0];
                    $els = explode("x", $p);
                    if ($els[1] > $els[0]) { // It's portrait

                    } else { // It's landscape
                        echo("<a class='dropdown-item' href='/download.php?v=" . $_GET['v'] . "&res=" . $p . "'>" . str_replace("x", "×", $p) . "</a>");
                    }
                }

                ?></div>
            </div>
        </span>
    </div>
</div>

<style>

    html, body {
        overflow: hidden;
    }

    .navbar {
        background-color: rgba(52, 58, 64, .75) !important;
    }

</style>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/private/footer.php"; ?>
